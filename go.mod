module bitbucket.org/carlostrub/bapu

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gizak/termui v2.1.2-0.20161129151807-f63e0cdd3f66+incompatible
	github.com/kolo/xmlrpc v0.0.0-20150413191830-0826b98aaa29
	github.com/maruel/panicparse v0.0.0-20160720141634-ad661195ed0e // indirect
	github.com/maruel/ut v1.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.2-0.20161012013512-737072b4e32b // indirect
	github.com/mitchellh/go-wordwrap v0.0.0-20150314170334-ad45545899c7 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/nsf/termbox-go v0.0.0-20161205194251-abe82ce5fb7a // indirect
	github.com/pelletier/go-buffruneio v0.1.0 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
